/* Becky Pier - CS 110A
inclass_3.8.12.cpp
Program outputs the largest number from a file.
*/

#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

int main()
{

	ifstream input_file;
	input_file.open("scorefile.txt");
	
	if(!input_file)
		{
		cout << "Unable to open scorefile.txt\n";
		exit(1);
		}
		
		float single_number, largest_number;
		input_file >> single_number;
		largest_number = single_number;
		
	while (!input_file.eof())
		{
			input_file >> single_number;
			if (single_number > largest_number)
				largest_number = single_number;
		}
	
	cout << "The highest number in scorefile.txt is : " << largest_number << endl;
	
	return 0;
}
		
