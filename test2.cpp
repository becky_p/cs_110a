/* 
Becky Pier
CS 110A, Assignment
A text-based game, inspired by the classic computer movie War Games. Game displays a simple simulation of the results of a nuclear war between the United States and the Soviet Union.
global_thermonuclear_war.cpp
*/

#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

char askUserToPlay();
int chooseSides();
int chooseAndAttackTargets();
void generateCounterAttack(int side, int number_of_targets);

int main() {
    cout << "Greetings Professor Falken.\n";
    
	while (askUserToPlay() == 'N') {
    	cout << "Are you sure you don't want to play?  People sometimes make mistakes. \n";
    }
    
    int side = chooseSides();
    
    cout << "Awaiting first strike command. \n";
    
    int test;
    test = chooseAndAttackTargets();
    
    return 0;
}

char askUserToPlay() {
	char wanna_play;
	cout << "Would you like to play a game? (Y/N) \n";
	cin >> wanna_play;
	
	wanna_play = toupper(wanna_play);
	return wanna_play;
	}
	
int chooseSides() {
	int user_side;
	cout << "Which side do you want? \n";
	cout << "1: United States \n2: Soviet Union \n";
	cin >> user_side;
	
	if (user_side < 1 || user_side > 2) {
		cout << "Invalid input. Try again.\n";
		chooseSides();
		}
	
	return user_side;
	}

int chooseAndAttackTargets() {

	const int MAX_SIZE = 200;
	char target_names[MAX_SIZE][MAX_SIZE];
	cout << "Please choose primary targets by city and/or country name: \n";
	
	int i = 0;
		do {
		cin.getline(target_names[i], MAX_SIZE);
		i++;
		} while (target_names[i-1][0] != 0);
	
	return i;


	}
		
