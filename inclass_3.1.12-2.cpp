/*
Becky Pier
CS 110A, in class exercise
"hello" loop program
*/

#include <iostream>
using namespace std;

int main()
{

	int num_hellos;

	cout << "How many times should I say hello?\n";
	cin >> num_hellos;

	do
	{
		if(num_hellos % 2 == 0)
			cout << "hello ";
		else cout << "HELLO ";
		num_hellos --;
	}
	while(num_hellos>0);
	
	cout <<"\n";
	
	return 0;
}
