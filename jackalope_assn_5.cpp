/* Becky Pier
CS 110A, Practice Problem 5
jackalope_assn_5.cpp
Program calculates jackalope population for a given number of generations, based on user input.
*/

#include <iomanip>
#include <iostream>
using namespace std;

int main()
{
	char calculate_again;
	do
	{
		int starting_pop, num_generations;
		const double birth_rate = .03, death_rate = .01;
	
		cout << "How many jackalopes do you have? ";
		cin >> starting_pop;
	
		cout << "How many generations do you want to wait? ";
		cin >> num_generations;
	
		double pop_plus_births, ending_pop;
		int current_pop = starting_pop;
	
		for (int i = 1; num_generations >= i; i++)
		{
			pop_plus_births = static_cast<int>(current_pop + (current_pop * birth_rate));
			ending_pop = (pop_plus_births - static_cast<int>(pop_plus_births * death_rate));
			current_pop = ending_pop;
		}
	
		cout << "If you start with " << starting_pop << " jackalopes and wait "; 
		cout << num_generations << " generation(s), you'll end up with a total of ";
		cout << ending_pop << " jackalopes.\n";
	
		cout << "Do you want to calculate another population? (Y/N): \n";
		cin >> calculate_again;
		calculate_again = toupper(calculate_again);
		
	}while(calculate_again=='Y');
	
	return 0;
}

/* Sample Output

becky@ada:~/Desktop/CS_110A$ ./a.out
How many jackalopes do you have? 200
How many generations do you want to wait? 1
If you start with 200 jackalopes and wait 1 generation(s), you'll end up with a total of 203 jackalopes.
Do you want to calculate another population? (Y/N): 
y
How many jackalopes do you have? 132
How many generations do you want to wait? 2
If you start with 132 jackalopes and wait 2 generation(s), you'll end up with a total of 137 jackalopes.
Do you want to calculate another population? (Y/N): 
Y
How many jackalopes do you have? 40
How many generations do you want to wait? 100
If you start with 40 jackalopes and wait 100 generation(s), you'll end up with a total of 287 jackalopes.
Do you want to calculate another population? (Y/N): 
n
becky@ada:~/Desktop/CS_110A$ 

*/

		
