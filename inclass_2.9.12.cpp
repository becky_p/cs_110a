/*
Becky Pier
In Class exercise 2.7.12
Calculates age differences. 
*/

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main ()
{
	char first_person_name[51], second_person_name[51];

	cout << "Please enter first person's name: ";
	cin >> first_person_name;

	cout << "Please enter second person's name: ";
	cin >> second_person_name;

	int first_person_age, second_person_age;

	cout << "Please enter first person's age: ";
	cin >> first_person_age;

	cout << "Please enter second person's age: ";
	cin >> second_person_age;

	bool first_person_working_age = (first_person_age >= 18 && first_person_age <= 65);
	bool second_person_working_age = (second_person_age >=18 && second_person_age <=65);
	
	if (first_person_working_age == true && second_person_working_age == true)
		cout << "Both " << first_person_name << " and " << second_person_name << " are between ages 18-65.\n";
	 else if (first_person_working_age == true && second_person_working_age == false)
	 	cout << first_person_name << "is between ages 18-65, but " << second_person_name << " is not.\n";
	 else if (first_person_working_age == false && second_person_working_age == true)
	 	cout << second_person_name << " is between ages 18-65, but " <<first_person_name << " is not.\n.";
	 else if (first_person_working_age == false && second_person_working_age == false)
	 	cout << "Neither " << frst_person_name << " nor " << second_person_name << " are between ages 18-65.\n.";
	
	return 0;

}
