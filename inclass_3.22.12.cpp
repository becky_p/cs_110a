/* Becky Pier - CS 110A
inclass_3.6.12.cpp
Program prints a rectangle 5 lines tall of a repeating single character. User inputs width and character.
*/

#include <iomanip>
#include <iostream>
using namespace std;

void draw_rectangle(int width, char character);

void user_input() // *****asks user to enter rectangle length and draw character
{
	char draw_character;
	int rect_width;
	int rect_length;

	cout << "Please enter a character: ";
	cin >> draw_character;
	
	cout << "Please enter the width of the rectangle: ";
	cin >> rect_width;
	
	draw_rectangle(rect_width, draw_character);
}
	
void draw_rectangle(int width, char character) // *****draws the rectangle
{
	for (int length = 1; length <=5; length++)
		{
			for (int i = 0; i <= width; i++)
				cout << character;
	
			cout << "\n";
		}
}

int main()
{
	user_input();
	
	return 0;
}
