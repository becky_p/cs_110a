/* Becky Pier CS 110A
In class exercise 1.26.12
110A_1.27.12.cpp
*/

# include <iostream>
using namespace std;

int main()
{

double grade_1 = 90.0;
double grade_2 = 81.2;
double grade_3 = 88.77;

cout <<"The three test grades are: " <<grade_1<< ", " <<grade_2<< ", and " <<grade_3<< endl;

double sum_of_grades;
double average_grade;

sum_of_grades = grade_1 + grade_2 + grade_3;
average_grade = sum_of_grades / 3;
cout <<"The average of the three grades is: " <<average_grade<< endl;

return 0;
}
