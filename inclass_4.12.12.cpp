/*
Becky Pier, CS 110A
In Class Exercise 4.12.12
Input the ages of 5 people into an array, and output the array in reverse order.
inclass_4.12.12.cpp
*/

#include <iostream>
using namespace std;

int main() {

	const int NUMBER_OF_AGES = 5;
	int ages_of_people[5];
	for (int i = 0; i < NUMBER_OF_AGES; i++) {
		cout << "Please enter the age of person " << i + 1 << ": \n";
		cin >> ages_of_people[i];
	}
	
	for (int i = NUMBER_OF_AGES - 1; i >= 0; i--) {
		cout << "The age of person " << i + 1 << " is :" << ages_of_people[i] << endl;
	}
	return 0;
}
