/* Becky Pier
CS 110A
Assignment 2
110A_2.cpp
*/

#include <iostream>
using namespace std;

int main()
{
	cout << "Hello, World!\n"; //Display the string.
	return 0;
}

/*
becky@ada:~/Desktop$ ./a.out
Hello, World!
becky@ada:~/Desktop$ 
*/
