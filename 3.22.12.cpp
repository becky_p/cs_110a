/* Becky Pier - CS 110A
inclass_3.6.12.cpp
Program prints a rectangle 5 lines tall of a repeating single character. User inputs width and character.
*/

#include <iomanip>
#include <iostream>
using namespace std;

void user_input() // asks user to enter rectangle length and draw character
{
	char draw_character;
	int rect_width;
	int rect_length;

	cout << "Please enter a character: ";
	cin >> draw_character;
	
	cout << "Please enter the width of the rectange: ";
	cin >> rect_width;
	
	void draw_rectangle(int rect_width, char draw_character);
}
	
void draw_rectangle(int rect_width, char draw_character) // draws the rectangle
{
	for (rect_length = 1; rect_length <=5; rect_length++)
		{
			for (int i = 0; i <= rect_width; i++)
				cout << draw_character;
	
			cout << "\n";
		}
}

int main()
{
	user_input();
	
	return 0;
}
