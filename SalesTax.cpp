/*
Becky Pier
CS 110A Programming Lab 1
Calculates monthly sales tax based on input from user.  Output is saved as a file.
SalesTax.cpp
*/

#include <iostream>
#include <cstdio>
#include <iomanip>
#include <fstream>

using namespace std;

int main ()
{

	char month_of_report[10], year_of_report[5];

	cout << "Please enter the month for this report: ";
	cin >> month_of_report;

	cout << "Please enter the year for this report: ";
	cin >> year_of_report;

    char name_of_file[256]; // name of output file reflects month and year of report
	snprintf(name_of_file, 256, "Sales_Tax_Data_%s_%s.txt", month_of_report, year_of_report);

	double state_sales_tax_rate, county_sales_tax_rate;
	state_sales_tax_rate = .04;
	county_sales_tax_rate = .02;

	double total_month_income, state_sales_tax, county_sales_tax, total_sales_tax, month_product_sales;
	cout << "Please enter the total income for this month: ";
	cin >> total_month_income;

	month_product_sales = total_month_income / (1 + state_sales_tax_rate + county_sales_tax_rate);
	state_sales_tax = month_product_sales * state_sales_tax_rate;
	county_sales_tax = month_product_sales * county_sales_tax_rate;
	total_sales_tax = state_sales_tax + county_sales_tax;

	ofstream out_stream;
	out_stream.open(name_of_file);
	out_stream << fixed << setprecision(2) << showpoint;

	out_stream << "Month: ";
	out_stream << setw(10) << right << month_of_report << " " << year_of_report << "\n";
	out_stream << "-----------------------\n";

	out_stream << setw(23) << left << "Total Collected:";
	out_stream << "$" << setw(9) << right << total_month_income << "\n";

	out_stream << setw(23) << left << "Sales:";
	out_stream << "$" << setw(9) << right << month_product_sales << "\n";

	out_stream << setw(23) << left << "County Sales Tax:";
	out_stream << "$" << setw(9) << right << county_sales_tax << "\n";

	out_stream << setw(23) << left << "State Sales Tax:";
	out_stream << "$" << setw(9) << right << state_sales_tax << "\n";

	out_stream << setw(23) << left << "Total Sales Tax";
	out_stream << "$" << setw(9) << right << total_sales_tax << "\n";

	out_stream << "-----------------------\n";

	out_stream.close();

	cout << "Sales Tax Report Saved to file: " << name_of_file << "\n";

	return 0;
}

/*
becky@ada:~/Desktop$ ./a.out
Please enter the month for this report: February
Please enter the year for this report: 2005
Please enter the total income for this month: 15000.25
Sales Tax Report Saved to file: Sales_Tax_Data_February_2005.txt
becky@ada:~/Desktop$ cat Sales_Tax_Data_February_2005.txt
Month:   February 2005
-----------------------
Total Collected:       $ 15000.25
Sales:                 $ 14151.18
County Sales Tax:      $   283.02
State Sales Tax:       $   566.05
Total Sales Tax        $   849.07
-----------------------
becky@ada:~/Desktop$ ./a.out
Please enter the month for this report: October
Please enter the year for this report: 2004
Please enter the total income for this month: 26572.89
Sales Tax Report Saved to file: Sales_Tax_Data_October_2004.txt
becky@ada:~/Desktop$ cat Sales_Tax_Data_October_2004.txt
Month:    October 2004
-----------------------
Total Collected:       $ 26572.89
Sales:                 $ 25068.76
County Sales Tax:      $   501.38
State Sales Tax:       $  1002.75
Total Sales Tax        $  1504.13
-----------------------
*/
