/* 
!!!!!ALL CODE written by Becky Pier.!!!!!
CS 110A, Assignment 7
A text-based game, inspired by the classic computer movie War Games. Game displays a simple simulation of the results of a nuclear war between the United States and the Soviet Union.
global_thermonuclear_war.cpp
*/

#include <iostream>
#include <iomanip>
using namespace std;

char askUserToPlay();  // Asks user if they want to play a game. 
int chooseSide(); // Asks user to choose the "side" (country) they will play as.
int chooseAndAttackTargets(); // Users type in strings (up to 50) to select targets. 
int generateCounterAttack(int num_targets, char cities[][20]); // Counterattacks cities on user's side by looping through array.
void displayEndingMessage(int num_targets, int num_counterattack_targets); //displays game results.

int main() {
    cout << "Greetings Professor Falken.\n";
	while (askUserToPlay() == 'N') {
    	cout << "Are you sure you don't want to play?  People sometimes make mistakes. \n";
    }
    int side = chooseSide();
    cout << "Awaiting first strike command. \n";
    int num_targets = chooseAndAttackTargets();
	const int MAX_NUM_LETTERS = 20;
	char US_CITIES[][MAX_NUM_LETTERS] = {"Juneau", "Denver", "New York City", "Detroit", "Chicago", "San Francisco", "Colorado Springs", "Wichita", "Los Angeles", "Miami", "Boston", "Washington D.C.", "Seattle", "Atlanta", "Houston", "Dallas", "Little Rock", "Honolulu", "Cheyenne", "Portland", "Eugene", "Santa Fe", "Albuquerque", "Salt Lake City", "Baton Rouge", "New Orleans", "Mountain View", "St. Louis", "Kansas City", "Boise", "Bozeman", "Tulsa", "Tacoma", "Raleigh", "Charleston", "Hartford", "Montgomery", "Anchorage", "Indianapolis", "Phoenix", "Dover", "Springfield", "Frankfort", "Green Bay", "Minneanapolis", "Jackson", "Lincoln", "Las Vegas", "Concord", "Trenton", "Fargo"};
	char SOVIET_CITIES[][MAX_NUM_LETTERS] = {"Moscow", "Tbillsi", "Donestk", "Leninsk", "Ashgabat", "Dunshabe", "Bishkek", "Almaty", "Volgograd", "Minsk", "Vilnius", "Kiev", "Chardzhou", "Svedlovsk", "Yakutsk", "Tynda", "Khabarovsk", "Lensk", "Yakutsk", "Evensk", "Omsk", "Frunze", "Nukus", "Lvov", "Riga", "Vorkuta", "Novyy Urengoy", "Alma-Ata", "Bratsk", "Karaganda", "Irkutsk", "Guryev", "Tura", "Leningrad", "Dikson", "Novaya Zemyla", "Murmansk", "Sverdlovsk", "Chelyabinsk", "Yerevan", "Provideniya", "Anadyr", "Cherskiy", "Kotlas", "Magadan", "Vladivostok", "Semipalatinsk", "Chita", "Komsomolsk", "Norilsk," "Odessa" };
	int num_counterattack_targets;
	if (side == 1) {
		num_counterattack_targets = generateCounterAttack(num_targets, US_CITIES);
	}
	else {
		num_counterattack_targets = generateCounterAttack(num_targets, SOVIET_CITIES);
	}
	displayEndingMessage(num_targets, num_counterattack_targets);
    return 0;
}

// *****Asks user if they want to play a game. Users don't really have a choice, unless they force quit. Mwahaha.
char askUserToPlay() {
	char wanna_play;
	cout << "Would you like to play a game? (Y/N) \n";
	cin >> wanna_play; // change this to cin.get?
	wanna_play = toupper(wanna_play);
	return wanna_play;
}

// *****Asks user to choose an int to represent the "side" they would like to play.
int chooseSide() {
	int side;
	cout << "Which side do you want? \n";
	cout << "1: United States \n2: Soviet Union \n";
	cin >> side;
	if (side < 1 || side > 2) {
		cout << "Invalid input. Try again.\n";
		chooseSide();
	}
	cout << endl;
	return side;
}
	
// ***** Users can type in up to 50 strings, one for each target.
// Strings are stored in 2d array. Function loops back through stored array to "nuke" each target.
int chooseAndAttackTargets() {
	const int MAX_SIZE = 51; // 50 missiles, + 1 for sentinel value
	char target_names[MAX_SIZE][MAX_SIZE];
	cout << "Please choose primary targets by city and/or country name: \n";
	int i = 0;
	cin.ignore(1); // clears input buffer so cin.getline will work
		do {
			cin.getline(target_names[i], MAX_SIZE);
			i++;
			if (i == MAX_SIZE - 1) {
				cout << "Sheesh, someone's a bit nuke-happy. \n";
				cout << "Well, this is all the missiles you've got.\n";
			}
		} while (target_names[i-1][0] != 0); // stops loop when user inputs blank line
	int num_targets = --i; // subtracts the blank line at the end
	for (int j = 0; j < num_targets; j++) {
		cout << "Nuking..." << target_names[j] << endl;
		}
	cout << endl;
	return num_targets;
}

// ***** Loops through array of cities from user's side to generate counter attack.
int generateCounterAttack(int num_targets, char cities[][20]) {
	int num_counterattack_targets = num_targets + 1;
	cout << "Oh noes! The other side is launching a counterattack. \n\n";
	for (int i = 0; i < num_counterattack_targets; i++) {
		cout << "Nuking..." << cities[i] << endl;
	}
	cout << endl;
	return num_counterattack_targets;
}

// Displays ending message. (Protip: you can't win this game.)
void displayEndingMessage(int num_targets, int num_counterattack_targets) {
	cout << "You nuked " << num_targets << " cities ";
	cout << "but your opponent nuked " << num_counterattack_targets << " of your cities.\n\n";
	cout << "A strange game. The only winning move is not to play.\n";
}

/* Sample Output:
becky@ada:~/cs_110a$ ./a.out
Greetings Professor Falken.
Would you like to play a game? (Y/N) 
N
Are you sure you don't want to play?  People sometimes make mistakes. 
Would you like to play a game? (Y/N) 
Y
Which side do you want? 
1: United States 
2: Soviet Union 
1

Awaiting first strike command. 
Please choose primary targets by city and/or country name: 
Moscow
St. Petersburg
Vilnius
Minsk

Nuking...Moscow
Nuking...St. Petersburg
Nuking...Vilnius
Nuking...Minsk

Oh noes! The other side is launching a counterattack. 

Nuking...Juneau
Nuking...Denver
Nuking...New York City
Nuking...Detroit
Nuking...Chicago

You nuked 4 cities but your opponent nuked 5 of your cities.

A strange game. The only winning move is not to play.

becky@ada:~/cs_110a$ ./a.out
Greetings Professor Falken.
Would you like to play a game? (Y/N) 
y
Which side do you want? 
1: United States 
2: Soviet Union 
9
Invalid input. Try again.
Which side do you want? 
1: United States 
2: Soviet Union 
2


Awaiting first strike command. 
Please choose primary targets by city and/or country name: 
San Francisco
Colorado Springs
Milwaukee
Fargo
Seattle
Concord
Florida
Denver

Nuking...San Francisco
Nuking...Colorado Springs
Nuking...Milwaukee
Nuking...Fargo
Nuking...Seattle
Nuking...Concord
Nuking...Florida
Nuking...Denver

Oh noes! The other side is launching a counterattack. 

Nuking...Moscow
Nuking...Tbillsi
Nuking...Donestk
Nuking...Leninsk
Nuking...Ashgabat
Nuking...Dunshabe
Nuking...Bishkek
Nuking...Almaty
Nuking...Volgograd

You nuked 8 cities but your opponent nuked 9 of your cities.

A strange game. The only winning move is not to play.

*/
