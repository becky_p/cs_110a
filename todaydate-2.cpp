/* Becky Pier CS 110A
todaydate-2.cpp
*/

# include <iostream>
using namespace std;

int main()
{
	cout <<"Please enter the day of the month: ";
	int day_of_the_month;
	cin >> day_of_the_month;
	cout <<"Please enter the month as a numerical value and press Enter. For example, January would be 1 :";
	int month_of_the_year;
	cin >> month_of_the_year;
	cout <<"The day of the month is %s." ,day_of_the_month;
	cout <<"The month of the year is %s." ,month_of_the_year;

	return 0;
}

