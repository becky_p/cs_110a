/* Becky Pier - CS 110A
inclass_3.6.12.cpp
Program prints a rectangle 5 lines tall of a repeating single character. User inputs width and character.
*/

#include <iomanip>
#include <iostream>
using namespace std;

int main()
{

	char input_character;
	int rect_width;
	int rect_length;

	cout << "Please enter a character: ";
	cin >> input_character;
	
	cout << "Please enter the width of the rectange: ";
	cin >> rect_width;
	
	for (rect_length = 1; rect_length <=5; rect_length++)
		{
			for (int i = 0; i <= rect_width; i++)
				cout << input_character;
	
			cout << "\n";
		}

	return 0;
}
