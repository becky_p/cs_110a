/*
Becky Pier
CS 110A, Programming Lab 4
Calculates how long it will take to pay off a loan, based on user input.
loan_calculator.cpp
*/

#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

// Asks user to enter principle, annual interest rate, and monthly payment amount.
void getUserInput(double &principle, double &annual_interest_rate, double &monthly_payment);

// Makes user re-enter data if they enter zero or negative numbers.
double validateInput();

// Calculates total interest paid, amount of final payment, how many months it will take to pay off the loan.
void calculateLoanPayoffTerms(double principle, double annual_interest_rate, double monthly_payment);

int main() {
	cout << "** Welcome to the Consumer Loan Calculator. \n\n";
	double principle, annual_interest_rate, monthly_payment;
	getUserInput(principle, annual_interest_rate, monthly_payment);
	calculateLoanPayoffTerms(principle, annual_interest_rate, monthly_payment);
	cout << "** Don't get overwhelmed with debt! **\n";
	return 0;
}

// ***** Asks user to enter principle, annual interest rate, and monthly payment amount.
void getUserInput(double &principle, double &annual_interest_rate, double &monthly_payment) {
	cout << "How much do you want to borrow? $";
	principle = validateInput();
	cout << "What is the annual interest rate expressed as a percent? (For example, if interest rate was 15.75 percent, you enter \"1575\" without the quotes.) ";
	annual_interest_rate = validateInput();
	cout << "What is the monthly payment amount? $";
	monthly_payment = validateInput();
}

// Calculates total interest paid, amount of final payment, how many months it will take to pay off the loan.
void calculateLoanPayoffTerms(double principle, double annual_interest_rate, double monthly_payment) {
	double monthly_interest_rate, monthly_interest_amount, loan_balance, number_of_months, final_payment, total_interest_amount;
	loan_balance = principle;
	monthly_interest_rate = (annual_interest_rate / 12) / 100;
	total_interest_amount = 0;
	for (number_of_months = 0; loan_balance > 0; number_of_months++) {
		monthly_interest_amount = loan_balance * monthly_interest_rate;
		total_interest_amount = monthly_interest_amount + total_interest_amount;
		if (monthly_interest_amount >= monthly_payment) {
			cout << fixed << showpoint << setprecision(2) << "You must make payments of at least $" << monthly_interest_amount + 1;
			cout << "\nbecause your monthly interest amount is $" << monthly_interest_amount << endl << endl;
			return;
		}
		loan_balance = (loan_balance + monthly_interest_amount) - monthly_payment;
		if (loan_balance < 0) {
			final_payment = monthly_payment - (loan_balance * -1);
		}
	}
	cout <<  "Your debt will be paid off after " << number_of_months << " months, ";
	cout << fixed << showpoint << setprecision(2) << "with a final payment of just $" << final_payment << endl << "The total amount of interest you will pay during that time is $" << total_interest_amount << endl << endl;
}

// *****Checks validity of user data. If user has entered invalid data (<= 0), they must re-enter.
double validateInput() {
	double user_input;
	cin >> user_input;
	if (user_input <= 0) {
		cout << "You must enter a positive number!\n";
		cout << "Please re-enter: \n";
		user_input = validateInput();
	}
	return user_input;
}

/* Sample Output:
becky@ada:~/cs_110a$ ./a.out
** Welcome to the Consumer Loan Calculator. 

How much do you want to borrow? $1000
What is the annual interest rate expressed as a percent? (For example, if interest rate was 15.75 percent, you enter "1575" without the quotes.) 18
What is the monthly payment amount? $50
Your debt will be paid off after 24 months, with a final payment of just $47.83
The total amount of interest you will pay during that time is $197.83

** Don't get overwhelmed with debt! **
becky@ada:~/cs_110a$ ./a.out
** Welcome to the Consumer Loan Calculator. 

How much do you want to borrow? $15000
What is the annual interest rate expressed as a percent? (For example, if interest rate was 15.75 percent, you enter "1575" without the quotes.) 10
What is the monthly payment amount? $100
You must make payments of at least $126.00
because your monthly interest amount is $125.00

** Don't get overwhelmed with debt! **
becky@ada:~/cs_110a$ ./a.out
** Welcome to the Consumer Loan Calculator. 

How much do you want to borrow? $-50
You must enter a positive number!
Please re-enter: 
-200
You must enter a positive number!
Please re-enter: 
20000
What is the annual interest rate expressed as a percent? (For example, if interest rate was 15.75 percent, you enter "1575" without the quotes.) -2.5
You must enter a positive number!
Please re-enter: 
5
What is the monthly payment amount? $0
You must enter a positive number!
Please re-enter: 
200
Your debt will be paid off after 130 months, with a final payment of just $125.79
The total amount of interest you will pay during that time is $5925.79

** Don't get overwhelmed with debt! **
becky@ada:~/cs_110a$ 
*/

