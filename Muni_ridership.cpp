/*
Becky Pier
CS 110A Practice Problem 3
Program calculates average estimated Muni ridership, based on data input from user.
Muni.ridership.cpp
*/

# include <iostream>
using namespace std;

int main()
{
	cout <<"Welcome to the Muni Ridership Calculator.\n";
	char muni_line[29];
	cout <<"Which Muni line did you survey?";
	cin >> muni_line;
	
	int days_surveyed;
	cout <<"How many days did you survey it? ";
	cin >> days_surveyed;
	
	int rider_count;
	cout <<"How many riders did you count? ";
	cin >> rider_count;
	
	float avg_ridership;
	avg_ridership = static_cast<float>(rider_count) / days_surveyed;
	
	cout <<"According to your survey, an average of "<< avg_ridership << " people rode the "<< muni_line << " per day.\n";
	
	return 0;
}

/*
becky@ada:~/Desktop$ ./a.out
Welcome to the Muni Ridership Calculator.
Which Muni line did you survey? K-Ingleside
How many days did you survey it? 5
How many riders did you count? 123456
According to your survey, an average of 24691.2 people rode the K-Ingleside per day.
becky@ada:~/Desktop$ ./a.out
Welcome to the Muni Ridership Calculator.
Which Muni line did you survey?N-Judah
How many days did you survey it? 30
How many riders did you count? 250000
According to your survey, an average of 8333.33 people rode the N-Judah per day.
becky@ada:~/Desktop$
*/

