/* Craig Persiko - CS 110A - Sample program
   InvestLoop.cpp

   A program to calculate the interest earned on an investment over time.
*/

#include <iomanip>
#include <iostream>
using namespace std;

int main()
{
  double balance;
  int yearsPassed;

  cout << "How much money do you want to invest? $";
  cin >> balance;

  for (yearsPassed = 1; yearsPassed <= 7; yearsPassed++)  // repeat each year for 7 years
  	{
    balance += balance * 0.05;  // add 5% interest to the balance.
    cout << "after " << yearsPassed << " years: " << balance << endl;
  	}
  cout << fixed << showpoint << setprecision(2);
  cout << "\nAfter 7 years at 5% interest, " << "you will have $" << balance << endl;
}
