/*
Becky Pier
CS 110A, Programming Lab 5
Determines whether a string entered by the user is a palindrome.
palindrome.cpp
*/
#include<iostream>
#include<cstring> 

// This function determines whether the input string from user is a palindrome.
bool determineIfWordIsAPalindrome(char word[21], int word_length);
// This function displays a message telling the user whether their string is a palindrome.
void displayMessage(char word[21], bool Is_Word_A_Palindrome, int word_length);

using namespace std;
int main() {
	cout << "Please enter a word: ";
	char word[21];
	cin.getline(word, 21);
	int word_length = strlen(word);
	bool Is_Word_A_Palindrome = determineIfWordIsAPalindrome(word, word_length);
	displayMessage(word, Is_Word_A_Palindrome, word_length);
	return 0;
}

//***** This function returns true if the word is a palindrome and false if the word is not.
bool determineIfWordIsAPalindrome(char word[0], int word_length) {
	int k = (word_length / 2);
	for (int i = 0, j = word_length-1; i < k; i++, j--) {
		if (toupper(word[i]) != toupper(word[j])) { //input comparison is case-insensitive
			return false;
		}
	}
	return true;
}

//***** This function displays a message telling the user if their word is a palindrome.
void displayMessage(char word[21], bool Is_Word_A_Palindrome, int word_length) {
	cout << "The word you entered, \"";
	for (int i = 0; i < word_length; i++) {
		cout << word[i];
		}
	cout << "\", is ";
	if (Is_Word_A_Palindrome == false) {
		cout << "not ";
		}
	cout << "a palindrome. \n";
}

/* Sample Output:

becky@ada:~/cs_110a$ ./a.out
Please enter a word: radar
The word you entered, "radar", is a palindrome. 
becky@ada:~/cs_110a$ ./a.out
Please enter a word: aBccBA
The word you entered, "aBccBA", is a palindrome. 
becky@ada:~/cs_110a$ ./a.out
Please enter a word: abcddxba
The word you entered, "abcddxba", is not a palindrome. 
becky@ada:~/cs_110a$

*/
