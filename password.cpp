/* Craig Persiko  -  CS 110A  -  Password Entry Program
   This program gets and validates a password for a user.
*/

#include <iostream>
#include <cctype>  // needed for islower function
#include <string>
using namespace std;

bool is_all_lowercase (string word);
// returns true if word has nothing but lower-case letters,
// else false (assumes word is a null-terminated string)

int main()
{

  // Passwords shouldn't be longer than 8 characters,
  // but allow extra room in the array in case user
  // enters too many letters.
  string uname, password, pass2;
  bool re_enter = false;

  cout << "\nWelcome to the Password Entry Program\n";
  cout << "\nEnter Username (no spaces allowed): ";
  cin >> uname;
  do
  {
    cout << "\nEnter Password (no spaces allowed): ";
    cin >> password;
    re_enter = password.length() < 5 || password.length() > 8;
    if (re_enter)
      cout << "Password must have at least 5 and at most "
           << "8 characters (no whitespace)";
    else  // Password length is okay
    {
      re_enter = is_all_lowercase(password);
      if (re_enter)
        cout << "Password must have at least 1 uppercase letter, "
             << "number, or punctuation mark.";
      else // Password is acceptable
      {
        cout << "Re-enter password: ";
        cin >> pass2;
        re_enter = (password.compare(pass2) != 0);
      }
    }
  }while(re_enter);
  cout << "Password set for user " << uname << endl;

  return 0;
}


// return true if word has nothing but lower-case letters,
// else false (assumes word is a null-terminated string)
bool is_all_lowercase(string word)
{
  bool all_lower = true;
  // true means all we've found so far are lower-case letters.

  // loop until we get to the end of the string ('\0' marks end)
  // or until we find a character that isn't lower case
  // (all_lower is true if that's all we've found)
  string::iterator it;
  for( it = word.begin(); it < word.end() && all_lower ; it++) {
    all_lower = islower(word.at(*it));
  return all_lower;
}
}

