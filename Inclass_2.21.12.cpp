/*
Becky Pier
In Class exercise 2.21.12
*/

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main ()
{

	int zip_code;
	cout << "Please enter your zip code: ";
	cin >> zip_code;

	int first_3_digits;

	first_3_digits = zip_code / 100;
	
	cout << "You live in the general vicinity of ";

	switch(first_3_digits)
	{
		case 900: case 901:
			cout << "Los Angeles.\n";
			break;
		
		case 921:
			cout << "San Diego.\n ";
			break;
		
		case 937:
			cout << "Fresno.\n";
			break;
		
		case 941:
			cout << "San Francisco.\n";
			break;
		
		case 946:
			cout << "Oakland.\n";
			break;
		
		default:
			cout << "Nowheresville, apparently!\n"; // Just kidding.
	}
	return 0;
}
