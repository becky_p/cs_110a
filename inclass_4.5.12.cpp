/*
Becky Pier
CS 110A, in class assignment
Programs determines birth year using reference parameters.
inclass_4.5.12.cpp
*/

#include<iostream>
#include<iomanip>
using namespace std;

bool getBirthInfo(int&userAge, int&currentYear);
void outputBirthYear(bool birthdayPassed, int userAge, int currentYear);

int main() {
	bool birthdayPassed;
	int userAge, currentYear;
	birthdayPassed = getBirthInfo(userAge, currentYear);
	outputBirthYear(birthdayPassed, userAge, currentYear);
	return 0;
}
// asks user to enter age, current year, and whether or not they've had a birthday yet.
bool getBirthInfo(int&userAge, int&currentYear) {
	cout << "How old are you? \n";
	cin >> userAge;
	cout << "What year is it now? \n";
	cin >> currentYear;
	char bdayInput;
	cout << "Did you have your birthday yet? (y/n)\n";
	cin >> bdayInput;
	bdayInput = toupper(bdayInput);
	if (bdayInput == 'Y')
		return true;
	else
		return false;
}
// *****outputs birth year.
void outputBirthYear(bool birthdayPassed, int userAge, int currentYear) {
	int birthYear;
	if (birthdayPassed == true)
		birthYear = currentYear - userAge;
	else
		birthYear = currentYear - userAge - 1;
	cout << "You were born in " << birthYear << ".\n";
}
	

