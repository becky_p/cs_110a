/*
Becky Pier
CS 110A Practice Problem 4
Program calculates bank fees, based on data input from user.
BankCharges.cpp
*/

# include <iostream>
# include <iomanip>

using namespace std;

int main()
{
	double beginning_balance;
	cout << "Enter the following information about your checking account.\nBeginning balance: $";
	cin >> beginning_balance;
	
	if (beginning_balance < 0)
	cout << "Your account is overdrawn!\n";
	
	int num_checks_written;
	cout << "Number of checks written: \n";
	cin >> num_checks_written;
	
	double checking_fee;
	
	if (num_checks_written >= 0 && num_checks_written < 20)
		checking_fee = num_checks_written * .1 ;
	else if (num_checks_written >= 20 && num_checks_written < 40)
		checking_fee = num_checks_written * .08;
	else if (num_checks_written >= 40 && num_checks_written <= 59)
		checking_fee = num_checks_written * .06;
	else if (num_checks_written >= 60)
		checking_fee = num_checks_written * .04;
	else if (num_checks_written < 0)
			{
			cout << "Error. Number of checks written must be 0 or greater. Please re-run the program and enter a valid value.\n";
			return 0;
			}
			
	double total_bank_fees = checking_fee + 10;
	double ending_balance = beginning_balance - total_bank_fees;
	
	if (beginning_balance < 400)
		ending_balance = ending_balance - 15;
	
	
	cout << "The bank fee this month is $" << showpoint << setprecision(4) << beginning_balance - ending_balance << "\n";
	
	if (beginning_balance > 0 && ending_balance < 0)
	cout << "Warning! Your account will be overdrawn after bank fees have been deducted.\n";
	
return 0;
}

/*
Sample Output
becky@ada:~/Desktop$ ./a.out
Enter the following information about your checking account.
Beginning balance: $1000
Number of checks written: 
5
The bank fee this month is $10.50
becky@ada:~/Desktop$ ./a.out
Enter the following information about your checking account.
Beginning balance: $100.50
Number of checks written: 
100
The bank fee this month is $29.00
becky@ada:~/Desktop$ ./a.out
Enter the following information about your checking account.
Beginning balance: $-5
Your account is overdrawn!
Number of checks written: 
-10
Error. Number of checks written must be 0 or greater. Please re-run the program and enter a valid value.
becky@ada:~/Desktop$ ./a.out
Enter the following information about your checking account.
Beginning balance: $405.25
Number of checks written: 
5
The bank fee this month is $10.50
becky@ada:~/Desktop$ 
*/


