/*
Becky Pier
In Class exercise 2.7.12
Calculates age differences. 
*/

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main ()
{
	char first_person_name[51], second_person_name[51];

	cout << "Please enter first person's name: ";
	cin >> first_person_name;

	cout << "Please enter second person's name: ";
	cin >> second_person_name;

	int first_person_age, second_person_age;

	cout << "Please enter first person's age: ";
	cin >> first_person_age;

	cout << "Please enter second person's age: ";
	cin >> second_person_age;

	cout << first_person_name << " is ";

	if (first_person_age != second_person_age)
		cout << "not ";

	cout << "the same age as " << second_person_name << ".\n";

	if (first_person_age > second_person_age)
		cout << first_person_name << " is older than " << second_person_name << ".\n";
	else if (second_person_age > first_person_age)
		cout << second_person_name << " is older than " << first_person_name << ".\n";
			
	return 0;

}
