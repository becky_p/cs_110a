/*
Becky Pier
In Class exercise 1.31.12
Program calculates the capacity of buses for an event, based on user input.
*/

# include <iostream>
# include <cmath>
using namespace std;

int main()
{

int expected_guests;
int buses_needed;
int unused_capacity;
const int bus_capacity = 40;

cout <<"How many guests are you expecting at the wedding?";
cin >> expected_guests;

buses_needed = (ceil(static_cast<double>(expected_guests)/ bus_capacity));
unused_capacity = (buses_needed * bus_capacity) - expected_guests;

cout <<"You will need " << (buses_needed) <<" bus(es) to transport all your guests.\n";
cout <<"A bus has the capacity to carry "<< bus_capacity <<" people, so you will have " << unused_capacity << " seats left over.\n";

return 0;
}



