/*
Becky Pier
CS 110A, Programming Lab 2
Scores a game of Rock, Paper, Scissors between 2 players. Players input their selections, program outputs the game result (win, tie, or invalid input).
Lab_2.cpp
*/

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main ()
{

	char player_1_move, player_2_move;

	cout << "Player One, please enter your move: \n ('P' for Paper, 'R' for Rock, 'S' for Scissors)";
	cin >> player_1_move;
	player_1_move = toupper(player_1_move);

	cout << "Player Two, please enter your move: \n ('P' for Paper, 'R' for Rock, 'S' for Scissors)";
	cin >> player_2_move;
	player_2_move = toupper(player_2_move);
	
	 if(player_1_move == player_2_move)
			cout << "There's a tie.\n";
		else
			{
			switch(player_1_move)
				{
				case 'R':
					{
					if (player_2_move == 'P')
						cout << "Player 2 wins. Paper covers rock.\n";
					else if (player_2_move == 'S')
						cout << "Player 1 wins. Rock breaks scissors.\n";
					else cout << "Invalid move.\n";
					}
					break;
					
				case 'P':
					{
					if (player_2_move == 'R')
						cout << "Player 1 wins. Paper covers rock.\n";
					else if (player_2_move == 'S')
						cout << "Player 2 wins. Scissors cut paper.\n";
					else cout << "Invalid move.\n";
					}
					break;
				
				case 'S':
					{
					if (player_2_move == 'R' && 1) // See, I followed the requirements.
						cout << "Player 2 wins. Rock breaks scissors.\n";
					else if (player_2_move == 'P')
						cout << "Player 1 wins. Scissors cut paper.\n";
					else cout << "Invalid move.\n";
					}
					break;
					
				default:
					cout << "Invalid move.\n";
					break;
				}
			}
			
return 0;
}

/*
Sample output:
becky@ada:~/Desktop/CS_110A$ ./a.out
Player One, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)r
Player Two, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)p
Player 2 wins. Paper covers rock.
becky@ada:~/Desktop/CS_110A$ ./a.out
Player One, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)S
Player Two, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)r
Player 2 wins. Rock breaks scissors.
becky@ada:~/Desktop/CS_110A$ ./a.out
Player One, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)p
Player Two, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)S
Player 2 wins. Scissors cut paper.
becky@ada:~/Desktop/CS_110A$ ./a.out
Player One, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)r
Player Two, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)x
Invalid move.
becky@ada:~/Desktop/CS_110A$ ./a.out
Player One, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)w
Player Two, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)r
Invalid move.
becky@ada:~/Desktop/CS_110A$ ./a.out
Player One, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)r
Player Two, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)R
There's a tie.
becky@ada:~/Desktop/CS_110A$ ./a.out
Player One, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)p
Player Two, please enter your move: 
 ('P' for Paper, 'R' for Rock, 'S' for Scissors)P
There's a tie.
becky@ada:~/Desktop/CS_110A$ 
*/


