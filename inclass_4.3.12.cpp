/* 
Becky Pier
CS 110A, in class assignment
Calculates average number of days missed per employee, based on user input.
inclass_4.3.12.cpp
*/

#include <iostream>
#include <iomanip>
using namespace std;

int getNumberOfEmployees();
int getNumberOfDaysMissed(int num_employees);
double calculateAverageDaysMissed(int num_employees, int total_days_missed);

int main() {
	int num_employees, total_days_missed;
	num_employees = getNumberOfEmployees();
	total_days_missed = getNumberOfDaysMissed(num_employees);
	double average_days_missed = calculateAverageDaysMissed(num_employees, total_days_missed);
	cout << "The average number of days missed per employee is ";
	cout << showpoint << setprecision(2) << average_days_missed << endl;
	return 0;
}

// ***** asks user to enter number of employees
int getNumberOfEmployees() {
	int num_employees;
	cout << "How many employees does the company have? \n";
	cin >> num_employees;
	while (num_employees < 1) {
		cout << "Invalid input. Number of employees must be one or greater.\n";
		num_employees = getNumberOfEmployees();
		}
	return num_employees;
	}

// ***** asks user to enter days missed for each employee
int getNumberOfDaysMissed(int num_employees) {
	int num_days_missed;
	int total_days_missed = 0;
	for (int i = 1; i <= num_employees; i++) {
		cout << "Enter days missed by employee #" << i << " :";
		cin >> num_days_missed;
		while (num_days_missed < 0) {
			cout << endl << "Invalid input. Days missed must be 0 or greater. Please re-enter: ";
			cin >> num_days_missed;
			}
		total_days_missed = total_days_missed + num_days_missed;
		}
	return total_days_missed;
	}
	
// ***** calculates average days missed
double calculateAverageDaysMissed(int num_employees, int total_days_missed) {
	double average_days_missed = static_cast<double>(total_days_missed) / num_employees;
	cout << average_days_missed;
	return average_days_missed;
	}
