/* Becky Pier
CS 110A, Lab 3
Program prints parallelogram of a repeating single character. User inputs side length and character.
parallelogram_assn_6.cpp
*/

#include <iostream>
using namespace std;

int main()
{
	cout << "This program will output a parallelogram. \n";
	
	int side_length;
	cout << "How long do you want each side to be? \n";
	cin >> side_length;
	
	char draw_character;
	cout << "Please enter the character you want it to be made of: \n";
	cin >> draw_character;
	
	for (int i = 1; i < side_length; i++)
		{
			for (int j = 0; j <= i; j++)
				cout << draw_character;
				
			cout << endl;
		}
	
	for (int k = side_length - 1, l = 1; k > 0; k--, l++)
	{
		for (int m = 0; m < l; m++)
			cout << " ";
	
		for (int n = k; n > 0; n--)
			cout << draw_character;
	
		cout << endl;
	}

	return 0;
}

/* Sample Output:
becky@ada:~/Desktop/CS_110A$ ./a.out
This program will output a parallelogram. 
How long do you want each side to be? 
6
Please enter the character you want it to be made of: 
@
@@
@@@
@@@@
@@@@@
@@@@@@
 @@@@@
  @@@@
   @@@
    @@
     @
becky@ada:~/Desktop/CS_110A$ ./a.out
This program will output a parallelogram. 
How long do you want each side to be? 
9
Please enter the character you want it to be made of: 
*
**
***
****
*****
******
*******
********
*********
 ********
  *******
   ******
    *****
     ****
      ***
       **
        *
becky@ada:~/Desktop/CS_110A$ 
*/
