/*
Becky Pier
In Class exercise 2.2.12
Outputs user name and average homework grade based on user input.
*/

#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main ()
{
	char full_name[51];
	cout << "Hi! Please enter your first and last name, separated by a space. (For example, Jane Student.) ";
	
	cin.getline(full_name, 51);
	
	float grade1, grade2, grade3;
	cout << "Please enter your first homework grade: ";
	cin >> grade1;
	cout << "Please enter your second homework grade: ";
	cin >> grade2;
	cout << "Please enter your third homework grade: ";
	cin >> grade3;
	
	float average_grade;
	average_grade = (grade1 + grade2 + grade3) / 3;
	
	cout << "Your name is " << full_name << " and your average grade is " << setprecision(1) << showpoint << fixed << average_grade<< "\n";
	
	ofstream out_stream;
	out_stream.open("name_and_grade.txt");
	out_stream << fixed;
	out_stream << setprecision(1);
	out_stream << showpoint;
	
	out_stream << full_name << endl;
	out_stream << average_grade;
	
	return 0;
}
	
