#include <iostream>
using namespace std;

int getPosNumber(); // checks user input to ensure that positive int is entered
void generateRectangle(int width, int height); // outputs rectangle on screen
void generateRow(int width, char first_char, char second_char); // outputs row characters on screen

int main() {
	int width, height;
	cout << "This program will output a checkerboard pattern.\n";
	cout << "How wide do you want it? ";
	width = getPosNumber();
	cout << "How tall? ";
	height = getPosNumber();
	generateRectangle(width, height);
	return 0;
}

// ***** This function outputs rectangle on the screen.
void generateRectangle(int width, int height) {
	char star = '*';
	char space = ' ';
	for(int i = 1; i <= height; i++) {
		if (i % 2 == 1) // output is different for even and odd rows
			generateRow(width, star, space);
		else
			generateRow(width, space, star);
	}
}

// ***** This function reads integer input from the console. If the number is not positive, the user is told to enter a new number until a positive number is entered.
int getPosNumber() {
	int user_input;
	cin >> user_input;
	if(user_input <= 0) {
		cout << "You must enter a positive integer. Please re-enter: ";
		user_input = getPosNumber();
	}
	return user_input;
}

//***** This function takes 2 char arguments so the checkerboard will be correctly spaced.
void generateRow(int width, char first_char, char second_char) {
	for(int j = 1; j <= width; j++) {
		if(j % 2 == 1) 
			cout << first_char;
		else
			cout << second_char;
	if(j == width)
		cout << endl;
	}
}
